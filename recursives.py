# -*- coding: utf-8 -*-
"""
Created on Sun Jan 30 15:32:33 2022

@author: lucas
"""
import datetime


def main():
    number = 10
    result = factorielle_recursive(number)
    print(result)
    
    
    start_time = datetime.datetime.now()
    fibonacci_iterative(number)
    end_time = datetime.datetime.now()
    time_diff = (end_time - start_time)
    execution_time = time_diff.total_seconds() * 1000
    print("-------------------------- fibonacci iterative: " , execution_time ,"--------------------------")
    
    start_time = datetime.datetime.now()
    fibonacci_recursive(number)
    end_time = datetime.datetime.now()
    time_diff = (end_time - start_time)
    execution_time = time_diff.total_seconds() * 1000
    print("-------------------------- fibonacci recursive: " , execution_time ,"--------------------------")
    
    
    
    
def factorielle_recursive(n):
    print(n)
    if n == 0:
        print("stop")
        return 1
    else :
        print("continue")
        print("n")
        return n * factorielle_recursive(n-1)
    
def factorielle_iterative(n):
    res=1
    if n==0:
        return res
    else :
        for i in range (1,n+1):
            res*=i
    return res

def fibonacci_recursive(n):
    if n==0:
        return 0
    elif n==1:
        return 1
    else :
        return fibonacci_recursive (n-1)+fibonacci_recursive(n-2)
    
def fibonacci_iterative(n):
    a=0
    b=1
    if n==0:
        return a
    elif n==1:
        return b
    else :
        for i in range(n-1):
            x=a+b
            a=b
            b=x
        return x

if __name__ == "__main__":
    main()