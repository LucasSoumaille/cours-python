# -*- coding: utf-8 -*-
"""
Created on Sun Jan 30 11:48:59 2022

@author: lucas
"""

import random

# définition de la taille max de la pile
len_pile = 10

def main():
    # création de la pile
    pile = []
    for i in range(12):
        element = random.randint(0, 100)
        print(element)
        pile = push(pile, element)
    for i in range(12):
        print(pile)
        pile = pop(pile) 
    print(pile)
    

def vide(pile): 
    # return true si la liste est vide sinon retourne false
    return len(pile) == 0

def pleine(pile):
    # return true si la liste a la même taille que len_pile, sinon retourne false
    return len_pile == len(pile)

def push(pile, elem):
    # vérifier si liste est pleine
    if pleine(pile):
        print ("pile pleine")
        return pile
    else :
        # si elle est n'est pas pleine ajout de l'élément à la fin
        pile.append(elem)
        return pile
    
def pop(pile):
    # vérifier si la liste est vide
    if vide(pile):
        print ("pile vide")
        return pile
    else :
        # si elle n'est pas vide, on affiche le dernier élément et on renvoit la nouvelle liste moins le dernier élément
        print(pile[-1])
        nouvelle_pile = pile[:-1]
        return nouvelle_pile
    
if __name__ == "__main__":
    main()



