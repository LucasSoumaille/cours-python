# -*- coding: utf-8 -*-
"""
Created on Sun Jan 30 14:37:16 2022

@author: lucas
"""
import random
import datetime
import math


def main():
    list_init =[]
    for i in range(1000):
        list_init.append(random.randint(0, 10000))
    avg_value = sum(list_init) / len(list_init)
   
    print(list_init)  
    
    print(avg_value)
    
    start_time = datetime.datetime.now()
    result_list = tri_selection(list_init)
    end_time = datetime.datetime.now()
    time_diff = (end_time - start_time)
    execution_time = time_diff.total_seconds() * 1000
    print("-------------------------- tri selection : " , execution_time ,"--------------------------")
    
    start_time = datetime.datetime.now()
    result_list = tri_insertion(list_init)
    end_time = datetime.datetime.now()
    time_diff = (end_time - start_time)
    execution_time = time_diff.total_seconds() * 1000
    print("-------------------------- tri insertion : " , execution_time ,"--------------------------")
    
    start_time = datetime.datetime.now()
    result_list = tri_bulle(list_init)
    end_time = datetime.datetime.now()
    time_diff = (end_time - start_time)
    execution_time = time_diff.total_seconds() * 1000
    print("-------------------------- tri bulle : " , execution_time ,"--------------------------")
    
    start_time = datetime.datetime.now()
    result_list = tri_fusion(list_init)
    end_time = datetime.datetime.now()
    time_diff = (end_time - start_time)
    execution_time = time_diff.total_seconds() * 1000
    print("-------------------------- tri fusion : " , execution_time ,"--------------------------")
    
    start_time = datetime.datetime.now()
    result_list = tri_quicksort(list_init, 0, len(list_init))
    end_time = datetime.datetime.now()
    time_diff = (end_time - start_time)
    execution_time = time_diff.total_seconds() * 1000
    print("-------------------------- tri quicksort : " , execution_time ,"--------------------------")
    
    
def tri_selection(input_list):
    for idx in range(len(input_list)):
        min_idx = idx
        for j in range(idx +1, len(input_list)):
            if input_list[min_idx] > input_list[j]:
                min_idx = j
        input_list[idx], input_list[min_idx] = input_list[min_idx], input_list[idx]
    return input_list


def tri_insertion(InputList):
    for i in range(1, len(InputList)):
        j = i-1
        nxt_element = InputList[i]
		
        while (InputList[j] > nxt_element) and (j >= 0):
            InputList[j+1] = InputList[j]
            j=j-1
        InputList[j+1] = nxt_element
    return InputList


def tri_bulle(input_list):

# Swap the elements to arrange in order
    for iter_num in range(len(input_list)-1,0,-1):
        for idx in range(iter_num):
            if input_list[idx]>input_list[idx+1]:
                temp = input_list[idx]
                input_list[idx] = input_list[idx+1]
                input_list[idx+1] = temp
    return input_list


# --------------- FUSION
def tri_fusion(unsorted_list):
    if len(unsorted_list) <= 1:
        return unsorted_list
# Find the middle point and devide it
    middle = len(unsorted_list) // 2
    left_list = unsorted_list[:middle]
    right_list = unsorted_list[middle:]

    left_list = tri_fusion(left_list)
    right_list = tri_fusion(right_list)
    return list(fusion(left_list, right_list))

# Merge the sorted halves

def fusion(left_half,right_half):
    res = []
    while len(left_half) != 0 and len(right_half) != 0:
        if left_half[0] < right_half[0]:
            res.append(left_half[0])
            left_half.remove(left_half[0])
        else:
            res.append(right_half[0])
            right_half.remove(right_half[0])
    if len(left_half) == 0:
        res = res + right_half
    else:
        res = res + left_half
    return res

# --------------- quicksort

def tri_quicksort(l,debut,fin):
    if fin >debut:
        k=partitionner(l,debut,fin)
        tri_quicksort(l,debut,k)
        tri_quicksort(l,k+1,fin)
    return l

def permuter(l,i,j):
    temp=l[j]
    l [ j]=l [ i ]
    l [ i]=temp

def partitionner(l,debut,fin):

    index_final_pivot=debut
    pivot=l[index_final_pivot]
    for i in range(debut+1,fin) :
        if l [ i]<pivot:
            permuter(l,i,index_final_pivot+1)
            index_final_pivot=index_final_pivot+1
            #print("i=",i)
            #print("l : ", l )
            #print("index_final_pivot=",index_final_pivot)
    permuter(l,debut,index_final_pivot)
    return index_final_pivot


if __name__ == "__main__":
    main()
    
    