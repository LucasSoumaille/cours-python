# -*- coding: utf-8 -*-
"""
Created on Sun Jan 30 11:48:59 2022

@author: lucas
"""

import random

# définition de la taille max de la file
len_file = 10

def main():
    # création de la file
    file = []
    for i in range(12):
        element = random.randint(0, 100)
        print(element)
        file = push(file, element)
    for i in range(12):
        print(file)
        file = pop(file)   
    print(file)
    

def vide(file): 
    # return true si la liste est vide sinon retourne false
    return len(file) == 0

def pleine(file):
    # return true si la liste a la même taille que len_file, sinon retourne false
    return len_file == len(file)

def push(file, elem):
    # vérifier si liste est pleine
    if pleine(file):
        print ("file pleine")
        return file
    else :
        # si elle est n'est pas pleine ajout de l'élément à la fin
        file.append(elem) # Recopie terme à terme à liste file
        return file
    
def pop(file):
    # vérifier si la liste est vide
    if vide(file):
        print ("file vide")
        return file
    else :
        # si elle n'est pas vide, on affiche le premier élément et on renvoit la liste mmoins le premier élément
        print(file[0])
        nouvelle_file=file[1:]
        return nouvelle_file
    
if __name__ == "__main__":
    main()



