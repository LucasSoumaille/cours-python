# -*- coding: utf-8 -*-
"""
Created on Sun Apr 25 17:13:43 2021

@author: lucas
"""

# Ajouter les valeurs d'un tableau (valeur_a_ajouter)
# à un autre tableau (valeurs_initiales) jusqu'à ce que
# la taille du tableau (valeurs_initiales) atteigne la taille voulue (taille_de_tableau)
# Si reverse est à True, les valeurs sont triés par ordre décroissant, sinon par ordre croissant

def main():
    # Déclaration des variables, il suffit de les modifiers pour que le programme s'adapte
    taille_de_tableau = 10
    valeurs_initiales = [1,2,3,4]
    valeur_a_ajouter = [10,20,30]
    # On peut changer la valeur du reverse ici pour changer le fonctionnement plus loin dans le code (ligne 41)
    # Comme pour les valeurs au dessus
    # reverse = True
    reverse = False
    result = add_value_to_array(taille_de_tableau, valeurs_initiales, valeur_a_ajouter, reverse)
    print(result)

def add_value_to_array(taille, tab1, tab2, ordre):
    tableau_temporaire = tab1
    # on utilise un compteur car si on utilise i on sera en dehors de la taille du tab2 
    compteur = 0
    for i in range(taille):
        tableau_temporaire.append(tab2[compteur])
        compteur += 1
        # Si le compteur atteint la taille du tableau 2, on le réinitialise pour pouvoir repartir du début
        if compteur == len(tab2):
            compteur = 0
    # on appelle la fonction qui permet de trier le tableau (en vrai c'est débile on pourrait juste mettre le ligne
    # 41 à la place mais c'est pour montrer qu'on peut passer des paramètres reçus à une autre fonction pour propager les valeurs)
    reverse_treatment(tableau_temporaire, ordre) 
    return tableau_temporaire

def reverse_treatment(tableau, is_reverse):
    tableau.sort(reverse=is_reverse)
    
if __name__ == '__main__':
    main()