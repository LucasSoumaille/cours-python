# -*- coding: utf-8 -*-
"""
Created on Sun Oct  3 22:41:31 2021

@author: lucas
"""
import string
import enchant

# Processus principal du programme
def main():
    # Récupération de l'alphabet
    alphabet_string = string.ascii_lowercase
    # Demande à  l'utilisateur les informations à récupérer
    sentence = input('Phrase : ')
    decalage = input('decalage : ')
    decodeOrEncode = input('decode ou encode (1 ou 2): ')
    # On appelle decode ou encode
    if decodeOrEncode == "1":
        decode(decalage, sentence, alphabet_string)
    else:
        encode(decalage, sentence, alphabet_string)
        
    toDecode = 'tawf bgmw sflgafw, lm sk ljgmnw ds kgdmlagf !'
    decodeWithoutDecal(toDecode, alphabet_string)
    

    # Fonction de decode sans connaitre decalage
def decodeWithoutDecal(sentence, alphabet):
    for i in range(len(alphabet)):
        new_sentence  = []
        int_decal = i
        for letter in sentence:
            if letter not in alphabet:
                new_sentence.append(letter);
            else :    
                index = 0
                if alphabet.index(letter) - int_decal < 0:
                    index = (alphabet.index(letter) - int_decal) + len(alphabet) 
                else:
                    index = alphabet.index(letter) - int_decal
                new_sentence.append(alphabet[index])
        print(''.join(new_sentence))
    

# Fonction de déchiffrement
def decode(decalage, sentence, alphabet):
    # Création buffer de la nouvelle phrase sous forme de tableau
    new_sentence  = []
    # On parse le decalage en int
    int_decal = int(decalage)
    # On parcours les éléments de la phrase
    for letter in sentence:
        # Si on ne connait pas la lettre on la garde tel quel
        if letter not in alphabet:
            new_sentence.append(letter);
        # Sinon on cherche la correspondance
        else :    
            index = 0
            # Si la la position - le décalage pour revenir à la lettre original "sort" de l'alphabet, on rajoute la longueur initiale de l'alphabet
            if alphabet.index(letter) - int_decal < 0:
                index = (alphabet.index(letter) - int_decal) + len(alphabet) 
            # Sinon on applique le décalage vers la gauche pour prendre la bonne lettre
            else:
                index = alphabet.index(letter) - int_decal
            # On ajoute la bonne lettre trouvé grâce à l'index
            new_sentence.append(alphabet[index])
    # On transforme le tableau en chaîne de caractère et on l'affiche
    print(''.join(new_sentence))
    
# Fonction de chiffrement
def encode(decalage, sentence, alphabet):
    new_sentence  = []
    int_decal = int(decalage)
    # Meme logique pour le déchiffrement mais à l'inverse
    for letter in sentence:
        if letter not in alphabet:
            new_sentence.append(letter);
        else :    
            index = 0
            # Au lieu de savoir si on "sort" de l'alphabet par la gauche (< 0), on cherche à savoir si on sort de l'alphabet par la droite (+25 ?)
            if int_decal + alphabet.index(letter) > len(alphabet) - 1:
                index = (alphabet.index(letter) + int_decal) - len(alphabet) 
            else:
                index = alphabet.index(letter) + int_decal
            new_sentence.append(alphabet[index])        
    print(''.join(new_sentence))

if __name__ == "__main__":
    main() 
    
    
