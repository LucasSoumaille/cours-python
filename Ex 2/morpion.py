# -*- coding: utf-8 -*-
"""
Created on Sun Feb 14 15:43:57 2021

@author: lucas
"""
import random

def generate_board(board_size):
    board = []  
    for y in range(board_size[0]):
        x_array = []
        for x in range(board_size[1]):
            x_array.append(' ')
        board.append(x_array)
    return board

def check_int(value):
    try: 
        int(value)
        return True
    except ValueError:
        print(ValueError)
        return False
    
def check_type_played(value):
    if value == 'X':
        return True
    else:
        return False
    
def check_possible(board, type_played, y, x, is_ia):
    is_possible = True
    if board[int(y)-1][int(x)-1] != ' ':
        is_possible = False
        if is_ia == False:
            print("Case déjà occupée, merci de sélectionner une autre case.")
    return is_possible

def play(board, type_played, y, x):
    board[int(y)-1][int(x)-1] = type_played;
    return board
    
def round_played(board):
    is_valid = False
    while is_valid == False:
        input_player = input("Jouez un X sur une case (format X,2,1 ou encore X,1,3) :")
        array_played = input_player.split(',')
        if len(array_played) == 3:
            if (check_int(array_played[1])):
                if (check_int(array_played[2])):
                    if int(array_played[1]) <= 3 and int(array_played[1]) >= 1 and \
                            int(array_played[2]) <= 3 and int(array_played[2]) >= 1:
                        if (check_type_played(array_played[0])):
                            if(check_possible(board, array_played[0], array_played[1], array_played[2], False)):
                                is_valid = True
                                board = play(board, array_played[0], array_played[1], array_played[2])
    return board;

def round_played_ia(board):
    type_played = "O"
    y = random.randint(1,3)
    x = random.randint(1,3)
    while check_possible(board, type_played, y, x, True) != True:
         y = random.randint(1,3)
         x = random.randint(1,3)
    print("L'ia a joué.")
    print("A vous de jouer.")
    board = play(board, type_played, y, x)
    return board

def is_win(board, pion):
    party_is_win = False
    if board[0][0] == pion and board[0][1] == pion and board[0][2] == pion or\
            board[1][0] == pion and board[1][1] == pion and board[1][2] == pion or\
            board[2][0] == pion and board[2][1] == pion and board[2][2] == pion or\
            board[0][0] == pion and board[1][0] == pion and board[2][0] == pion or\
            board[0][1] == pion and board[1][1] == pion and board[2][1] == pion or\
            board[0][2] == pion and board[1][2] == pion and board[2][2] == pion or\
            board[0][0] == pion and board[1][1] == pion and board[2][2] == pion or\
            board[0][2] == pion and board[1][1] == pion and board[2][0] == pion:
        party_is_win = True
        print("Partie gagnée pour le joueur : ", pion)
        print("fin de partie")
    return party_is_win;        

def main():
    board_size = [3,3]
    board = generate_board(board_size)
    print("La partie commence, vous êtes les X")
    end_party = False
    current_round = 0
    while end_party == False:
        for row in board:
            print(row)
            print("---------------")
        if is_win(board, "X") == True or is_win(board, "O") == True:
            end_party = True
        else :
            if current_round % 2 == 0:
                board = round_played(board)
            else :
                board = round_played_ia(board)
            current_round +=1

    
if __name__ == "__main__":
    main()    