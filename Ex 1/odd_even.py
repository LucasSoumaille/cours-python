# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import random
from statistics import mean
from statistics import median

# Question 1 : fonction de génération de liste, prends 3 paramètres
def generate_list(number_of_item, min_value, max_value): 
    list_value = []
    # Génère la liste en fonction du premier paramètre
    for i in range(number_of_item):
        list_value.append(random.randint(min_value, max_value))
    return list_value

# Question 2 : fonction de filtre les éléments dans deux listes pairs et impairs
def filter_even_odd(list_to_filter):
    # Déclaration des listes pair, impair, et liste englobante
    even_list = []
    odd_list = []
    final_list = []
    # pour chaque nombre dans la liste à filtrer 
    for number in list_to_filter:
        # Si le nombre est pair, je l'ajoute à la liste pair
        if (number % 2 == 0):
            even_list.append(number)
        # Sinon je l'ajoute à la liste impair
        else :
            odd_list.append(number)
    # Ajout des listes pair et impair dans la liste englbante
    final_list.append(odd_list);
    final_list.append(even_list)
    return final_list

# Question 3 : fonction de trie
def sort_list(lists_to_sort):
    # pour chaque liste dans la liste
    for list_to_sort in lists_to_sort:
        # Si le premier élément de la liste en cours est pair : Trie par ordre croissant
        if list_to_sort[0] % 2 == 0 :
            list_to_sort.sort()
        # Sinon True par ordre décroissant
        else:
            list_to_sort.sort(reverse=True)
    return lists_to_sort

# Question 4 : fonction pour enlever les éléments identiques
def remove_same_elements(list_to_remove):
    # Déclaration d'une nouvelle liste englobale vide
    list_return = []
    # Pour chaque liste dans la liste passée en paramètre
    for list_element in list_to_remove:
        # Déclaration de la nouvelle liste épurée qui sera remise à zéro à chaque nouvelle list_element dans list_to_remove
        result = []
        # Forme contractée : ajouter nombre unique, à réaliser pour chaque élément dans list en cours, si l'élément n'est pas dans la nouvelle liste
        [result.append(unique_number) for unique_number in list_element if unique_number not in result]
        # Ajouter la nouvelle dans la liste à renvoyer
        list_return.append(result)
    return list_return

# Question 5 : de création de json et de calcul de médiane et moyenne
def calculate_mean_median_to_json(list_to_calculate):
    # Déclaration du json et de son contenu, soit 3 listes, la liste initiale et 2 tableaux vides pour l'instant dans les clé lists, mean et median
    json_output = {}
    json_output['lists'] = list_to_calculate
    json_output['mean'] = []
    json_output['median'] = []
    # Pour chaque liste dont on doit calculer la médiane et la moyenne
    for list_to_calculate_median_mean in list_to_calculate:
        # Ajout de la moyenne arrondie au centième dans la clé mean du json, et de la médiane dans la clé median du json
        json_output['mean'].append(round(mean(list_to_calculate_median_mean),2))
        json_output['median'].append(median(list_to_calculate_median_mean))
    return json_output

# Processus principal (fonction main()) du programme à éxécuter, appel à toutes les fonctions en passant les paramètres souhaités
def main():
    number_item_in_list = 20
    minimum_value_to_generate = 0
    maximum_value_to_generate = 100
    print("--------------1st step--------------")
    list_value = generate_list(number_item_in_list, minimum_value_to_generate, maximum_value_to_generate)
    print(list_value)
    print("--------------2nd step--------------")
    list_filter_even_odd = filter_even_odd(list_value)
    print(list_filter_even_odd)
    print("--------------3rd step--------------")
    list_sort = sort_list(list_filter_even_odd)
    print(list_sort)
    print("--------------4th step--------------")
    list_remove = remove_same_elements(list_sort)
    print(list_remove)
    print("--------------5th step--------------")
    json_list = calculate_mean_median_to_json(list_remove)
    print(json_list)
    
# Si ces lignes existent, le programme exécute leur contenu, soit la fonction main() qui appelle les autres fonctions dans son processus
if __name__ == "__main__":
    main()