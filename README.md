# Cours pythons

Un ensemble d'exercices en Python pour amener la compréhension du langage de programmation.

Les exercices sont les suivants :
<ul>
  <li>Bases sur les listes, les fonctions mathématiques et les JSON</li>
  <li>Réalisation d'un Morpion</li>
  <li>Réalisation d'un Mastermind</li>
  <li>Mise en place d'un algo de césar</li>
  <li>Apprentissage des files/piles, fonctions de tri et récursives</li>
</ul> 