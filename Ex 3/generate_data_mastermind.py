# -*- coding: utf-8 -*-
"""
Created on Mon Apr 26 18:52:36 2021

@author: lucas
"""

import random

def write_file(path, content):
    File_object = open(path,"a")
    File_object.write(content)
    File_object.close()
    
def main():
    parties = 10000
    print("début")
    path_manches_file = "manches.csv"
    path_parties_file = "parties.csv"
    for i in range(parties):
        partie = i + 1
        # print('partie :', partie)
        player = random.randint(1, 2)
        # print("player :", player)
        win = 'T'
        win_int = random.randint(1, 2)
        if (win_int == 1):
            win = 'T'
        else :
            win = 'F'
        # print("win :", win)
        manche = 0
        if win_int == 1:
            manche = random.randint(1, 9)
        else : 
            manche = 10
        # print('manche : ', manche)
        content_partie = ""
        if win_int == 2 :
            content_partie = "\n" + str(player) + ";" + win +";"
        else :
            content_partie = "\n" + str(player) + ";" + win +";" + str(manche)
        for j in range(manche):
            current_manche = j+1
            # print("manche en cours :", current_manche)
            content_manche =''
            if win_int == 1 and j != manche-1 or win_int == 2:
                base = 4
                good = random.randint(0,3)
                not_good = random.randint(0, base - good)
                bad = base - good - not_good
                # print ("bon :", good)
                # print ("mauvais emplacement :", not_good)
                # print ("bad :", bad)
                content_manche = "\n" + str(partie) + ";" + str(player) + ";" + str(current_manche) +";" + str(good) +";" + str(not_good) +";" + str(bad)
            elif win_int == 1 and current_manche == manche:
                good = 4
                not_good = 0
                bad = 0
                # print ("bon :", good)
                # print ("mauvais emplacement :", not_good)
                # print ("bad :", bad)
                content_manche = "\n" + str(partie) + ";" + str(player) + ";" + str(current_manche) +";" + str(good) +";" + str(not_good) +";" + str(bad)
            write_file(path_manches_file, content_manche)
        
        write_file(path_parties_file, content_partie)
    print("OK")
            
                

if __name__ == "__main__":
    main()    