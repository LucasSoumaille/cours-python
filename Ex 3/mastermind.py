# -*- coding: utf-8 -*-
"""
Created on Sat Apr 24 18:49:42 2021

@author: lucas
"""
import numpy as np
import random

# faire Mastermind -- 
# 4 emplacements
# 5 types de pions 1 2 3 4 5qzdqz
# 10 manche max
# on doit rentrer 1-1-1-1 pour lancer un essai
# Si chiffre bien placé +, si chiffre mal placé *, si chiffre pas bon -
# créer un fichier csv avec en entête :
# joueur,victoire,nbrManches 

def main():
    emplacement = 4
    possible = [1,2,3,4,5]
    round_counter = 0
    max_round = 10
    win = False
    result_to_find = initialize(emplacement, possible)
    path_manches_file = "manches.csv"
    path_parties_file = "parties.csv"
    player = "0"
    game_number = read_file(path_manches_file)
    print(game_number)
    #print(result_to_find)
    while True:
        player = input("Choisir votre joueur (1 ou 2) :")
        if player == "1" or player =="2":
            break
    while not win:
        correct = 0
        bad = 0
        inccorect = 0
        round_counter += 1
        round_result = play_round(result_to_find, round_counter, emplacement)
        round_result_string = ''.join(round_result)
        correct = round_result.count("+")
        bad = round_result.count("*")
        inccorect = round_result.count("-")
        content = "\n" + str(game_number) + ";" + str(player) + ";" + str(round_counter) +";" + str(correct) +";" + str(bad) +";" + str(inccorect)
        write_file(path_manches_file, content)
        print('résultat : ',round_result_string)
        if(round_result_string == "++++"):
            win = True
            print("Vous avez gagné")
            content = "\n" + str(player) + ";" + "T" +";" +str(round_counter)
            write_file(path_parties_file, content)
            break
        elif(round_counter == max_round):
            print("Vous avez perdu")
            content = "\n" + str(player) + ";" + "F" +";"
            write_file(path_parties_file, content)
            break
    print('La partie est finie')
    print('le résultat était {}'.format(result_to_find))
    
def initialize(nbr_emplacement, type_possibles):
    result = []
    for i in range(nbr_emplacement):
        max_value = np.max(type_possibles)
        min_value = np.min(type_possibles)
        value_to_add = random.randint(min_value, max_value)
        result.append(value_to_add)
    return result

def play_round(result, manche, emplacement):
    input_value = input('Manche {}/10, rentrez votre combinaison (format X-X-X-X):'.format(manche))
    check = False
    while not check :
        check = check_format(input_value, emplacement)
        if check:
            break
        else:
            input_value = input('Mauvais format : rentrez votre combinaison (format X-X-X-X):')
    input_value_array = input_value.split('-')
    round_result = []
    index_to_remove = []
    result_to_find_local = []
    for item in result:
        result_to_find_local.append(item)
    for i in range(len(input_value_array)):
        if(int(input_value_array[i]) == int(result_to_find_local[i])):
            round_result.append('+')
            index_to_remove.append(i)
    for index in sorted(index_to_remove, reverse=True):
        del input_value_array[index]
        del result_to_find_local[index]
    for i in range(len(input_value_array)):
        if int(input_value_array[i]) in result_to_find_local:
            round_result.append('*')
            result_to_find_local.remove(int(input_value_array[i]))
    while len(round_result) != emplacement:
        round_result.append('-')
    return round_result
    
def check_format(input_value, emplacement):
    try: 
        input_value_array = input_value.split('-')
        if len(input_value_array) != emplacement:    
            return False
        for i in range(len(input_value_array)):
            try:
                int(input_value_array[i])
            except Exception:
                return False
        return True
    except ValueError:
        return False
    
def write_file(path, content):
    File_object = open(path,"a")
    File_object.write(content)
    File_object.close()
    
def read_file(path):
    File_object = open(path,"r+")
    last_line = File_object.readlines()[-1]
    print(last_line)
    value_game = last_line.split(";")[0]
    try:
        int(value_game)
    except Exception:
        value_game = 0
    File_object.close()
    return int(value_game) + 1
        
    
if __name__ == '__main__':
    main()
    
    